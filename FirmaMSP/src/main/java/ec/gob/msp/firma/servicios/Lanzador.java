package ec.gob.msp.firma.servicios;

import java.io.IOException;

import eu.europa.esig.dss.DSSDocument;
import eu.europa.esig.dss.token.AbstractSignatureTokenConnection;
import eu.europa.esig.dss.token.Pkcs12SignatureToken;

public class Lanzador {

	public static void main(String[] args) {
		FirmaXmlXadesBWithSelfSignedCertificate firmador = new FirmaXmlXadesBWithSelfSignedCertificate();
		DSSDocument docXml = firmador.prepareXmlDoc();
		//String pkcs12TokenFile = firmador.getPathFromResource("/rca.p12");
		String pkcs12TokenFile = "/home/dmurillo/FIRMA/FIRMAEC/FIRMA/bolivar_david_murillo_uquillas.p12";
		AbstractSignatureTokenConnection signingToken = null;
		try {
			signingToken = new Pkcs12SignatureToken(pkcs12TokenFile,"Uni20141");
		} catch (IOException e) {
			System.out.println("Error en el token "+e.getMessage());
		}
		try {
			firmador.firmarXmlXadesBWithSelfSignedCertificate(docXml, signingToken);
		} catch (IOException e) {
			System.out.println("Error en firma "+e.getMessage());
		}

	}

}
